#!/ust/bin/env python3
# -*- coding:utf-8 -*-

#Crear variables
lista1 = [3.5, 7.3, 10.51, 5.42, 5.532]
lista2 = [3, 7, 10, 5, 5]
lista3 = ['hola', 'feo', 'tonto', 'adios']
print(lista1)
print(lista2)

#funcion calculo de promedio
def promedio(lista1):
    suma = 0
    #guardar suma en variable "suma"
    for i in range (0, 5):
        suma = suma + lista1[i]
    return float(suma)

#funcion transformar lista2 a los cuadrados correspondientes
def cuadrados(lista2):
    #elever uno por uno el numero de la lista
    for i in range (0, 5):
        lista2[i] = lista2[i] * lista2[i]
    return lista2
#funcion palabra mayor
def cuenta_letras(lista3):
    return max(lista3)
    



#llamar funcion
promedio = promedio(lista1) / 5
cuadrados = cuadrados(lista2)
palabra = cuenta_letras(lista3)

#imprimir valores
print(palabra)
print(cuadrados)
print(promedio)
    
